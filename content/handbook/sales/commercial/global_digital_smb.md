---

title: "Global Digital SMB"
description: "Overview of the Global Digital SMB Sales Model"
---

## Global Digital SMB Sales Model

(FY25 Global Digital SMB All-Hands Deck)[https://docs.google.com/presentation/d/1NIznURPvchLXw22X7JMlaAhKcwoiMUCQhIG2TpTGFUw/edit#slide=id.g2b17593787a_0_275]

All members of the Sales, Support, Billing and Deal Desk teams should familiarize themselves with the Global Digital SMB Model.
The model leverages team-level account alignment so SMB customers have a team of SMB Advocates to assist them. Every SMB Advocate on the team is equipped to work with any SMB customer as the sales point of contact.
The threshold for Global Digital SMB Accounts accounts is <$30k cARR, <100 employees, and excludes Ultimate Licenses and Japan. 


## Account and Opportunity Ownership

Vision: SMB Accounts will not be owned individually, but by the entire team. This is meant to ensure that SMB customers are supported at the right time, in a scalable way. Each SMB Advocate will have a shared responsibility for collaborating in a customer-centric  manner to provide a best-in-class experience to each SMB customer. 

### Accounts

 - All AMER SMB Accounts are owned by the AMER SMB Sales User.
 - All APAC SMB Accounts are owned by the APAC SMB Sales User.
 - All EMEA SMB Accounts are owned by the EMEA SMB Sales User. 
 - Note that in SMB only, AMER & APAC are considered one territory.

### Opportunities 

 - All AMER SMB Opportunities are owned by the AMER SMB Sales User.
 - All APAC SMB Opportunities are owned by the APAC SMB Sales User.
 - All EMEA SMB Opportunities are owned by the EMEA SMB Sales User. 


### High Value Accounts

Some Global Digital SMB Accounts are considered to be Tier 1/ High Value Accounts,  based on their cARR and LAM. These accounts, whilst owned by an SMB Sales User, will be managed in a 1:1 relationship between the SMB Advocate and the customer, via Cases. 

The criteria that determines whether an SMB account is a Tier 1 account or not, can be found below. The table also highlights what type of Cases will be created for SMB Accounts, based on their respective Tier.

| Tier | Criteria                                                 | Digital Touch | High Value Cases | High Value 90/180/270 checkins (fallback)   | Urgent Renewal & TRX Support Case | Churn & Contraction Mitigation Cases | Expansion Opportunities Cases | Inbound Request Cases |
|------|----------------------------------------------------------|---------------|------------------|---------------------------------------------|-----------------------------------|--------------------------------------|-------------------------------|-----------------------|
| 1    | cARR > $7k                                               | Yes           | Yes              | Yes (if no other engagement within quarter) | Yes                               | Yes                                  | Yes                           | Yes                   |
| 2    | cARR < $7k AND (cARR > $1k OR (cARR < $1k AND LAM > 10)) | Yes           | No               | No                                          | Yes                               | Yes                                  | Yes                           | Yes                   |
| 3    | cARR < $1k AND LAM < 10                                  | Yes           | No               | No                                          | Yes                               | No                                   | No                            | Yes                   |

(FY25 SMB Account Tiering)[https://docs.google.com/spreadsheets/d/18xFnbi4xuHJGqcKDWf9HDdd0iS7YZNt4n4QC33qK67E/edit#gid=0]

## Engaging with Global Digital SMB Accounts

Since all Global Digital & SMB Accounts are owned by generic SMB Sales Users, SMB Advocates will only engage with customers when specific customer events/ scenarios are triggered. Once one of these defined scenarios is triggered, a case will auto be created. An SMB Advocate will then pick up the case, and work it through to completion.

#### Why?
- Enables Advocates to focus on those SMB customers that require, or would benefit from, sales assistance.
- Removes low potential customers from view, since these customers will seldom trigger a case. 

#### How?
Cases are automatically created when;

- A customer requires sales assistance in order to transact.
- There is a high likelihood a customer will churn/ contract.
- There is a high likelihood that a customer is considering upgrading.
- If none of the above apply, and the customer is on auto renew, a case will NOT be created.

## Case Types

The cases that will be auto created, are split into 5 distinct categories;

Inbound Request - These occur when a customer hand raises, and requests assistance from GitLab.  The cases are labeled as High Priority. 
- Contact Sales Request
- Hand-Raise PQL
- Support Ticket
- SDR Created

Churn & Contraction Mitigation - These occur when the account in question has exhibited signs of low usage/ adoption. The cases are labeled as Medium Priority. 
- Underutilization
- High PtC
- Auto Renew recently turned off

Expansion Opportunities - These occur when the account in question has exhibited signs of growth, and that they are likely to expand. The cases are labeled as Medium Priority.  
- FO Opp
- FO Opp (Startup)
- High PtE
- 6Sense Growth Signal
- Overage with QSRs turned off
- Customer MQL

Urgent Renewal & TRX Support - These are created when a situation exists that means the renewal must be processed by an Advocate. The cases are labeled as Medium Priority. 
- PO Renewal (includes partner & alliance renewals)
- EoA Renewal w/ >25 users
- Multiyear Renewal
- Auto Renewal due to fail
- Overdue Renewals

High Value - These are only created for the highest spending accounts in the segment. 
- High Value Account
- High Value Account check in
- Future High Value Account

The case logic, context behind the case, and CTA can be viewed on the Case Trigger overview sheet. 

(FY25 SMB Case Triggers)[https://docs.google.com/spreadsheets/d/1ihpt5WDpoJmDWa5gA0eXvBJOgFOJgL1J_QxsBuDDowc/edit#gid=1223186811]


The case logic, context behind the case, and CTA can be viewed on the Case Trigger overview sheet. 

## Working with the Global Digital SMB Account Team

If a GitLab team member needs to loop in the Advocate team on a customer Account, they must create a case.  (Chatter messages sent to the AMER/ APAC/ EMEA Sales Users are not monitored).  

- Navigate to the *Account* in Salesforce.
- Click on *Cases*, New.
- Select *SMB Sales Case*, from the Record Type dropdown.
- Add the *Contact*, to the Contact Name lookup field.
- Add the *Opportunity*, to the Opportunity lookup field (if relevant). 
- Describe the ask of the AE in the case *Description*. Include any relevant links or resources.
- Select a *Type* that most closely matches the origin of your request. 
- Set the *Priority* to High. 
- Complete the *Subject* field.
- Select a *Case Reason* based on the customers needs.
- Check the *Assign using active assignment rules* checkbox, and click *Save*.
- This Case will now drop into the SMB Sales Queue, and will be actioned by a SMB Advocate.


## SMB Advocate Playbook

### SMB Advocate Case Workflow

(FY25: SMB Advocate Enablement)[https://docs.google.com/presentation/d/1IWgcHXbFN5UVNHCWXtF1fKj_9k5h5KlQGClY-2RbqUE/edit#slide=id.g12b319f6181_0_5]

- If a SMB Advocate has capacity, they will click on an open case in the AMER or EMEA SMB Sales Team Queue and assign it to themselves, by changing the Case Owner.
- High Priority cases should always be picked up first.
- Information pertaining to the case will be displayed in the Case Reason, Context, CTA & Description box.
- An Advocate must then update the Status to In Progress.(This shows the case is being worked, and takes it out of the queue). 
- To work the case, the SMB Advocate should contact the customer, with all activities logged on the contact/ account, via Outreach. The Next Steps fields on the case should also be updated to include the case next steps/ date. 
- (If the Advocate is working on an Expansion Opportunity case, any Add On or Growth opp they uncover should be manually created, and linked to the case in question, via the Opportunity lookup field).
- Once a Case is considered resolved, the Status should be set to one of the four Closed options, and the Resolution Action updated. This will automatically update the Date/ Time Closed field.
- If applicable, the linked opportunity should also be closed.
- At any time, a SMB Advocate can view their cases by navigating to the My Cases list view in Salesforce.
- When working cases, the following Outreach cadence should be followed.
- - Day 1 - Email
- - Day 3 - Call
- - Day 5 - Email
- - Day 7 - Email
- An Advocate should look to resolve/ close a case within 7 days of it being opened. The max time to resolve a case is limited to 15 days (except cases with the subject: High Value Case). 
- The Advocate must also adhere to the Required 10, to ensure that the correct case fields and opportunity fields are updated when picking up, working, and closing out a case.

## Required 10

The Required 10 acts a to do list which should be followed by all Advocates when picking up and working cases. 

(FY25: SMB Required 10)[https://docs.google.com/spreadsheets/d/1yhRsbS0K9s1Un9puBFF51nE0oT040558QQfad2yL_4c/edit#gid=0]

### Picking Up

Case: Status
- Should be set to In Progress, (then Closed - Resolved/ Duplicate/ Unresponsive once worked). 
Case: Owner
- Should always be set to the Advocate working the case.  Cases can be moved in rare circumstances i.e the end user wishes to converse in a language that you do not speak, but another Advocate does. 
Case: Contact
- Advocate to add/ update the main contact, as the case is being worked

### Managing

Case: Next Steps & Date
- To act in the same fashion as Opp: Next Steps. Field is mandatory.
Account: GitLab Admin added
- Only for cases related to an opportunity
- Must be added in order to move opp to closed won.
Opp: Close Date
- Only for cases related to an opportunity
- To help with forecasting
Opp: Renewal Risk Category
- Only for cases related to an opportunity
- To help with forecasting
Contact/ Account: Activity
- Must be logged via Outreach on the Contact

### Closing

Case: Resolution Action
- Should always be populated when closing out a case
Opp: Status
- Only for cases related to an opportunity
- To mimic the case i.e if the case is closed, the opp should be closed won or lost
Case: Feedback (Optional)
- A place where an Advocate can share feedback pertaining to the case itself, or the customers use case

## Case Closure Criteria

Closed - Resolved
- The Advocate was able to address the customers' concern/ transact the renewal.
Closed - Duplicate
- An Advocate has already addressed the customer's concern via a similar case.
Closed - Unresponsive
- After 15 days of trying to contact the end user, no response have been received.
Closed - N/A
- All other scenarios. 

## Use Case Specifics

### High Value Cases

Whilst Accounts are always owned by the regions SMB Sales User, High Value Cases attached to accounts (Tier 1 accounts only), are owned by the SMB Advocate. 

These cases are to remain open for the duration of the customer’s term (i.e they should NOT be closed out after 15 days of being created). They act as a reminder that these customers require continual attention and should be worked strategically. The primary aim of High Value Cases is to not let the customer churn. 

The Advocate is advised to update Next Steps/ Next Steps Date, and create future dated reminder Tasks, so that they plan their outreach throughout the year.

Note that as per the Tiering Criteria, these Accounts can still trigger other cases types, which should be worked and closed out within the 15 day period.  This will include High Value Check In Cases, which will remind the Advocate to touch base with the customer!

Upon successful renewal, all open cases will be closed (by the SMB Advocate via a clean-up report) and a new High Value case will be opened for the next renewal. 
Inbound Request
Support Ticket cases will often include a Zendesk ticket link in the description field.  This should be viewed before contacting the customers. 
Referral cases often originate from other Sales segments.  To determine if the account and opp in question should be moved to SMB, chatter Sales Support on the account.

### FO Opp (an SDR engages with a net new lead who has been qualified)
Before picking up the case, the Advocate checks that they can attend the meeting (IQM) that appears as an Activity on the Contact record, as well as the Next Steps fields.
The Advocate should also advise the SDR, by chattering them on the opp, that they have picked up the case.
The below SDR Handover Process steps, and Handover Criteria, detailed below should also be followed by both the SDR and Advocate.

### SDR Created (an SDR engages with a lead that is related to a customer account)
Before creating a case, the SDR should convert the lead to a contact, under the customer account. 
When creating the case, this newly created Contact should be added to the Case. 
The below SDR Handover Process steps, and Handover Criteria, detailed below should also be followed by both the SDR and Advocate.

(FY25 SDR Handover Process)[https://docs.google.com/spreadsheets/d/18tGE_dpbx7nLu2S6bzD3WkLrHM3IrDv72WWJMgDDi64/edit#gid=0]

### Expansion Opportunities
If an opportunity is uncovered whilst working an Expansion Opportunity case, a new Growth/ Add On opp should be created, and linked to the case via the Opportunity lookup field.
If the customer is looking to upgrade to Ultimate/ add users at the time of renewal, the existing Renewal opp should be linked to the case.  The renewal opp should also be used for any new quotes that are generated. 

## Email Templates
In order to standardize outreach, and increase the efficiency in which Advocates can engage with cases.  We have pre-set email templates that can be used, based on which Case trigger you are engaging with. 
The email templates, and which Case Trigger they are relevant to, are listed in the below doc (as well as on the Case Trigger overview sheet). 

(SMB Email Templates)[https://docs.google.com/document/d/1eQgdVObeQe5_xMdBeAKiWOzIaWZstWInodScm2Lm924/edit#heading=h.qzkw31beo6xk]

## Case Hygiene 

### Case SLA

All High Priority cases should be picked up and responded to within 24 hours of entering the queue.

All Medium/ Low Priority cases should be picked up and responded to within 48 hours of entering the queue.

### Open Case Guidelines

All cases should be worked, and closed out, within a 15 day period (except High Value cases).

The above, and other data points, will be tracked on the below dash.  Periodically, the info will be presented to the Advocate so that they can ‘clean up their room’.

Due to the high case volume, it is imperative that the Required 10 is followed, so cases do not become stale. 

### Case Reason Dictionary

- Account Question / Update
- Adding Licenses / Purchasing Help
- Billing / Payment
- Licensing / Fulfillment
- Off-Cycle Subscription Changes (no uplift)
- Other
- Renewal
- Sales Evaluation (non-renewal)
- Technical Question
- Usage / Best Practices
- New High Engagement Account - 5* PtE
- New High Engagement Account - Tier 1 Uptier

### Case Resolution Action Dictionary

- Answered Question
- Recommended Resource
- Transferred To Support
- Growth Conversation
- Directed to Online Purchase
- Completed Sales Assisted Order
- Request Not Possible
- No Action Required
- Failed to Engage

## Dashboards & Reports

- (SMB AMER Case Queue)[https://gitlab.lightning.force.com/lightning/r/Report/00OPL000000LaDN2A0/view?queryScope=userFolders]
- (SMB EMEA Case Queue)[https://gitlab.lightning.force.com/one/one.app#/sObject/00OPL000000LaRt2AK/view]
- (SMB AMER Team Dash)[https://gitlab.lightning.force.com/lightning/r/Dashboard/01ZPL000000UbBp2AK/view?queryScope=userFolders]
- (SMB EMEA Dash)[https://gitlab.lightning.force.com/lightning/r/Dashboard/01ZPL000000WJU92AO/view?queryScope=userFolders]
- (SMB Advocate Dash)[https://gitlab.lightning.force.com/lightning/r/sObject/01ZPL000000VuM52AK/view?queryScope=userFolders]


## FAQ

- Q. Will cases be added to the queue everyday?
- A. Yes, we expect new cases to be created daily, and to total approximately 400 per month.
- Q. Can I pick up any Case in the queue?
- A. No, High Priority cases must be picked up first. If there are no High Priority cases, cases must then be worked oldest first.
- Q. How many cases should I work at any give time?
- A. An Advocate should only own 50 In Progress cases at a time.  Once at 50, the Advocate is deemed to be at capacity. 
- Q. Where can I provide feedback on this model/ process?
- A. Please submit all feedback via the #XXXX slack channel.  If you want to share Feedback relating to a specific case, please enter the feedback in the Case: Feedback field. 
- Q. Is additional enablement material available?
- A. Yes, please see the content below;
- Q. As an GitLab Team Member who is not part of the SMB Team, how do I engage with the team?
- A. Follow the process above entitled *Working with the Global Digital SMB Account Team*. 
- Q.  What happens if the end user speaks a different language than I do?
- A. Discuss with your manager who might be best placed in the team to handle this case, then switch the Case Owner to that individual. 
- Q. As an Advocate, if I pick up a FO Opp case, and discover the customer is actually a MM account, what do I do?
- A. Follow the below steps;
  - If the FO Opp is still in Stage 0, advise the SDR via Chatter that they should loop in the MM AE.
  - Mark the case as Closed - Duplicate
  - If the IQM has taken place, and you have moved the opp to Stage 1, loop in the relevant MM AE on the opp, via Chatter.
  - Closed the case as Closed - Duplicate.
- Q. There is no opportunity linked to the case, but I have identified a Growth/ Add On opportunity, what do I do?
- A. If you discover a Growth opportunity, you should manually create a Growth/ Add On opp and link it to the case via the Opportunity field. If an opp is already attached to the case (Renewal or QSR opp), this opp should be updated to reflect the possible growth. 
- Q. If I am working a case, and the customer advises they are looking to expand but not for 6 months, should I close out the case?
- A. No, update the Next Steps stating that the customer is open to conversation in 6 months.  Also change the Next Steps Date to 6 month from todays date.  Finally set to Case Owner to your territories SMB Sales User (AMER or EMEA). The case will subsequently drop back into the queue in 6 months times. 
- Q. I am an AE not in the SMB Org, and a former customer has reached out to me.  This customer is now owned by an SMB Sales User.  How do I hand this customer over to the SMB Sales Team?
- A. Follow this (process)[https://docs.google.com/document/d/1Bc9N0Cvc65NjWHSVoliUp6injZNlbkjEEEZ0dYQm_Ck/edit#heading=h.wmdr1tkj670j].  
- Q. I just closed won on opp for a SMB Customer, which took them above the $30k CARR threshold.  How do I hand this over to MM?
- A. See this (doc)[https://docs.google.com/document/d/1dIJwPRo3rQnE1e4LBG-lT4ZvJ3nXzMuKwP8mF54jACo/edit]. 
- Q. If I pick up a case, and the customer is interested in speaking in 6 months, do I keep the case open, or close it out?
- A. In this scenario, the case should be put back in the queue, and become available to be picked up again in 6 months time.  To do this;
  - Update the Next Steps on the case, as per your conversation with the customer.
  - Set to Next Steps Date to 6 months from today.
  - Set the Case Status to Open
  - Check the *Assing using active assignment rules checkbox*.
  - The case will now reappear in the queue in 6 months time, and any Advocate will be free to work the case.
- Q. If a prospect/ customer emails me directly requesting assistance, what do I do?
- A. First check the account to see if there is an In Progress SMB Sales Case owned by a different Advocate.  If there is, add the nature of the customers request to the Description field, and chatter the Advocate on the case. If no case is present on the opp, follow the above process (*Working with the Global Digital SMB Account Team*.), and assign the case directly to yourself. 

## Escalation Paths

### Solutions Architect

An Advocate should talk to their Manager to determine if SA support is necessary and available. 

### Support

Loop in supportteam@gitlab.com, and set the Resolution Action of the current open case to Transferred to Support. 

### Transitioning Accounts to Mid-Market / Enterprise
Chatter Sales Support, advising that the account in question may need to be re routed to the correct team. 

## SMB Advocate Onboarding + Enablement

### By Case-Type

#### First Order

- (Propensity to Purchase - Deck)[https://gitlab.highspot.com/items/6565e315ec1191ac7d8a26c0]
- (Propensity to Purchase - Training)[https://gitlab.highspot.com/items/65046a91a7f7915154f1ba95]
- (Doing a great discovery call)[https://gitlab.highspot.com/items/65365f760c1cf20725653853]

#### Expansion/Upgrade

- (Propensity to Expand)[https://gitlab.highspot.com/items/6565e314ec1191ac7d8a26b4#1]
- (PTE & PTC Scores Walkthrough Video)[https://gitlab.highspot.com/items/65365570bdabc0e8c24015b2]
- (Creating a great Ultimate Pitch - INTERNAL ONLY)[https://gitlab.highspot.com/items/65365dced498b9134400a185]

#### Churn/Contraction Mitigation
- (Propensity to Contract/Churn)[https://gitlab.highspot.com/items/6565e314ec1191ac7d8a26b4#1]
- (PTE & PTC Scores Walkthrough Video)[https://gitlab.highspot.com/items/65365570bdabc0e8c24015b2]

#### Renewal and TRX Support
- (Overview of Renewals at GitLab (internal handbook, log in via SSO))[https://internal.gitlab.com/handbook/sales/go-to-market/renewals/]




