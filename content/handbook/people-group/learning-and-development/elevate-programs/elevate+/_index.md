---

title: Elevate+

---

Leaders at GitLab enable our mission that [everyone can contribute](https://handbook.gitlab.com/handbook/company/mission/). We need to equip our people leaders with the skills to lead globally dispersed, all-remote teams to support our business growth at scale.

Through small group cohorts and interactive workshops, Elevate+ is designed to enable senior leaders at GitLab to better understand and practice behaviours anchored in GitLab's values and leadership competencies, develop new skills to support high priority business needs, and help establish and strengthen cross-functional connections.

Elevate+ will create a shared leadership development experience across the organization so that every leader has a solid capability to lead in alignment with our GitLab values. This program will focus on the leadership behaviors required to build high performing teams that deliver results for customers.

In Elevate+, participants will:

1. Understand and practice behaviors and frameworks anchored in GitLab’s values and leadership competencies
1. Apply new skills to support their highest priority business needs
1. Build cross-division relationships and learning through small group cohorts
1. Collaborate with stakeholders to integrate their learnings into their day-to-day work in order to tackle real challenges
1. Take action on feedback from their team, leader, and cross-functional stakeholders to map out your and your teams’ future growth

## What concepts are taught in Elevate+?

Elevate+ is organized into 5 learning modules. Read more about the skills covered in each module below:

![visual displaying the content in each elevate+ module](../elevateplus.png)


## Time Committment

![visual displaying the elevate+ round 1 timeline](../elevateplustimeline.png)

Elevate+ runs for a total of 6 months and is organized into 5 learning modules. In each module, participants spend approx. 4 hours in the following learning activities:

5 Total Modules

| Step | Week | Title | Description | Time |
| ----- | ----- | ----- | ---------- | ----- |
| 1 | 1 | Live Learning Session (Zoom) | Cross-functional synchronous learning session on the relevant module topic | 50 minutes |
| 2 | 2, 3 | Asynchronous Intersession work (GitLab) | Asynchronous assignment to reinforce concepts learned in the Live Learning Session and support participants in advancing their identified business goals. Each assignment will include conversations led by participant with identified individuals in order to directly apply new concepts and a documented deliverable due prior to the Live Cohort Workshops | 75 minutes |
| 3 | 4 | Live Cohort Workshop (Zoom) | Cross-functional synchronous cohort workshop sessions with ~8-10 other GitLab people leaders to review Intersession work, give and get feedback on application of concepts, and additional opportunities for practice | 80 minutes |
| 4 | 5 | Asynchronous Module Assessment (Level Up) | Short quiz in Level Up to demonstrate your understanding and application of new concepts | 15 minutes |


## Certification

1. During each module, participants will complete and submit their intersession work, as described above This work will demonstrate learning of the models / competencies, as well as implementation of difficult conversations / coaching.
1. Throughout the program, participants will revisit and adapt their ReadMes, crystallizing their focus on leadership development and the ways in which they contribute to GitLab’s business.
1. During certification, participants will:
     - present their leadership development plans to members of their cohort
     - commit to an accountability plan
     - answer any questions that arise from their peers and coach

More details on the exact format of certification will be shared soon. As much as possible, certification will be designed such that participants have choice in what format / structure they utilize.

## Making up missed sessions

Attendance to all Elevate+ live sessions is required to earn the certification. We understand that making time for all live sessions can be difficult. We have options for making up sessions you’ve missed:

**Option 1: Attend Another Session**

There are multiple Live Learning and Cohort Workshop options for each module. If you cannot attend your scheduled sessions, Option 1 is to attend another scheduled session.

**Option 2: Async Makeup**

If none of the Live Learning sessions will work, you may use Option 2: Async Makeup. Note that this can only be used a total of one time per participant.

Async makeups will be shared with participants directly via Level Up. The include watching recordings from the Live Learning and responding to text-based discussion threads in Level Up.

There is no async makeup option for Cohort Workshops.


## Measuring Success

How we’ll measure program impact:

**Self-Assessment:** Participants will complete a self-assessment, reflecting upon your current abilities and confidence as a leader of leaders. After the program, participants will re-take the assessment so you can see how they’ve grown.

**Stakeholder Assessment:** Identified stakeholders will receive an (anonymous!) survey to gather feedback both for participants to identify areas they want to focus on during the next 6 months as well as overall program feedback. Stakeholders will be surveyed again 6 months after the program to measure collective leadership growth of the Elevate+ participants. This is a fantastic opportunity to collect and take action on feedback from those you work closely with.

Who sees assessment results?

1. The Elevate program team will look at cumulative results, vs individual results.
1. Stakeholder results will be shared with you directly for use in the program.

## Frequently Asked Questions

### What does success in Elevate+ look like?

To earn the final Elevate certification, participants must:

1. Attend or makeup all live sessions
1. Complete all async intersession work
1. Pass the Elevate+ Certification

### Is my participation in Elevate+ required?

Yes, this and Elevate are both a required training for all people leaders at GitLab.

### Where do I communicate my questions, concerns, and feedback as I go through the program?

When the program begins, you’ll be added to a Slack group that includes all members of your cohort. You can also reach out directly to your manager or the Learning and Development team.

### I missed, or cannot attend, a live session. What do I do?

It’s a requirement to make up missed sessions within each month to ensure that you’re learning each new skill and moving through the program in the appropriate order. Please make every effort to attend live sessions as scheduled. Please review makeup options outline above.

We’re committed to working with you to find a solution that let’s you successfully complete Elevate. If you must miss one live session for personal reasons, you need approval from your manager. Additional missed sessions will result in an incomplete Elevate status.

### What if I have already completed the manager challenge?

The Manager Challenge is a past iteration of leadership development at GitLab. We appreciate the time that past manager challenge participations have dedicated to their growth and development. Elevate is the next iteration in our GitLab manager journey, and is still required of manager challenge participants.

### When I complete this program will I earn a certificate?

Yes! Upon successful completion of the program, including attendance to all required live sessions, asynchronous learning, and assessments, participants will be awarded the GitLab Elevate+ certification.

### How often will this program run?

Our goal is to enroll all elidigible managers into Elevate+ during FY25.

### Is there opportunity to participate in the program if I am an aspiring manager?

Due to our priority to certify people leaders, participation by aspiring managers is not currently an option. We’ve put together a Level Up course available to all GitLab Team Members, to share the self-paced training content for each Elevate (please note, there is not yet an async option for Elevate+). You can check it out here: [Elevate Learning Materials](https://levelup.gitlab.com/access/saml/login/internal-team-members?returnTo=https://levelup.gitlab.com/learn/learning-path/elevate-learning-materials)




